﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default1.aspx.cs" Inherits="DavidLauzon30.default1" %>

<!DOCTYPE html>
<html>
<head>
    <title>30 ans! David Lauzon</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.0.4/dist/jquery.countdown.min.js"></script>
</head>
<body style="background-image: url(rabbit.gif); background-repeat: repeat;">
<header>
</header>
<main>
    <% 
        var birthday = new DateTime(2016, 6, 4);
    %>
    <header class="content-header">
        <div class="container" style="color: #778899; text-align: center;">
            <h1>

                <i class="fa fa-code"></i>
                <%
                    if (DateTime.Now.Ticks < birthday.Ticks)
                    {
                        %>
                        
                            Décompte avant le 31 ans du petit prince adoré, David
                        <%
                    }
                    else
                    {
                    }
                 %>

            </h1>
            <img src="Dave312.jpg" height="400px"/>
        </div>
    </header>
    <div class="container" style="text-align: center;">
        <div class="pure-g-r">

            <div class="examples pure-u-3-5
        ">
                <article>

                    <div class="example-blocks">
                        <div id="clock" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
                    </div>

                        <%
                            if (DateTime.Now.Ticks > birthday.Ticks)
                            {
                        %>
                            <h1>Bonne Fête!! Dave</h1><br/>
                            <img src="cake.gif"/>
                        <%  
                            }
                            else
                            {

                            
                        %>
                        
                    <script type="text/javascript">
                        var birthday = new Date(2016, 6, 4);
                        $("#clock").countdown(birthday).on("update.countdown", function (event) {
                            var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                        });
                        
                    </script>
                    <br/>
                    <img src="Varigrip2.jpg" height="350px" alt="Tomber en poussiere"/>
                    
                        <%
                            }

                        %>
                </article>
            </div>

        </div>
    </div>

</main>
<footer>
</footer>
</body>
</html>