﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="defaut2.aspx.cs" Inherits="DavidLauzon30.default1" %>

<!DOCTYPE html>
<html>
<head>
    <title>30 ans! David Lauzon</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.0.4/dist/jquery.countdown.min.js"></script>
</head>
<body style="background-image: url(wrong-unicorn-7.png); background-repeat: repeat;">
<header>
</header>
<main>

<div class="container" style="text-align: center;">
<div class="row">
<div class="col-md-3">
    <%
        var birthday = new DateTime(2017, 7, 4);
    %>
    <div>
        <h1>

            <i class="fa fa-code"></i>
            <%
                if (DateTime.Now.Ticks < birthday.Ticks)
                {
            %>

                Décompte avant les 32 ans du petit prince adoré, David
            <%
                }
            %>

        </h1>

        <div class="thumbnail">
            <img src="/Dave3.jpg"/>
        </div>
    </div>
    <div>
        <article>

            <div class="example-blocks">
                <div id="clock" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
            </div>

            <%
                if (DateTime.Now.Ticks > birthday.Ticks)
                {
            %>
                <h1>Bonne Fête!! Dave</h1><br/>
                <img src="cake.gif"/>
            <%
                }
                else
                {
            %>

                <script type="text/javascript">
                    var birthday = new Date(2017, 6, 4);
                    $("#clock")
                        .countdown(birthday)
                        .on("update.countdown",
                            function(event) {
                                var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                            });

                </script>
                <br/>

            <%
                }
            %>
        </article>
    </div>
</div>
<div class="col-md-3">
    <%
        var birthday2 = new DateTime(2017, 3, 28);
    %>
    <div>
        <h1>

            <i class="fa fa-code"></i>
            <%
                if (DateTime.Now.Ticks < birthday2.Ticks)
                {
            %>

                Décompte avant les 25 ans de Prude le maginifique
            <%
                }
            %>

        </h1>
        <div class="thumbnail">
            <img src="/Prude/prude2.jpg"/>
        </div>

    </div>
    <div>
        <article>

            <div class="example-blocks">
                <div id="clock2" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
            </div>

            <%
                if (DateTime.Now.Ticks > birthday2.Ticks)
                {
            %>
                <h1>Bonne Fête!! Marc-André</h1><br/>
                <img src="cake.gif"/>
            <%
                }
                else
                {
            %>

                <script type="text/javascript">
                    var birthday2 = new Date(2017, 2, 28);
                    $("#clock2")
                        .countdown(birthday2)
                        .on("update.countdown",
                            function(event) {
                                var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                            });

                </script>

            <%
                }
            %>
        </article>
    </div>
</div>
<div class="col-md-3">
    <%
        var birthday3 = new DateTime(2016, 12, 7);
    %>
    <div>
        <h1>

            <i class="fa fa-code"></i>
            <%
                if (DateTime.Now.Ticks < birthday3.Ticks)
                {
            %>

                Décompte avant les 29 ans de Miguel O mayeur
            <%
                }
            %>

        </h1>

        <div class="thumbnail">
            <img src="Mich/Mic2.jpg"/>
        </div>

    </div>
    <div>
        <article>

            <div class="example-blocks">
                <div id="clock3" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
            </div>

            <%
                if (DateTime.Now.Ticks > birthday3.Ticks)
                {
            %>
                <h1>Bonne Fête!! Michael</h1><br/>
                <img src="cake.gif"/>
            <%
                }
                else
                {
            %>

                <script type="text/javascript">
                    var birthday3 = new Date(2016, 11, 7);
                    $("#clock3")
                        .countdown(birthday3)
                        .on("update.countdown",
                            function(event) {
                                var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                            });

                </script>

            <%
                }
            %>
        </article>
    </div>
</div>
<div class="col-md-3">
    <%
        var birthday4 = new DateTime(2017, 3, 18);
    %>
    <div>
        <h1>

            <i class="fa fa-code"></i>
            <%
                if (DateTime.Now.Ticks < birthday4.Ticks)
                {
            %>

                Décompte avant les 32 ans de Gilligan/Le capitaine
            <%
                }
            %>

        </h1>
        <div class="thumbnail">
            <img src="/stef/IMG_0930.JPG"/>
        </div>
    </div>
    <div>
        <article>

            <div class="example-blocks">
                <div id="clock4" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
            </div>

            <%
                if (DateTime.Now.Ticks > birthday4.Ticks)
                {
            %>
                <h1>Bonne Fête!! Marc-André</h1><br/>
                <img src="cake.gif"/>
            <%
                }
                else
                {
            %>

                <script type="text/javascript">
                    var birthday4 = new Date(2017, 2, 18);
                    $("#clock4")
                        .countdown(birthday4)
                        .on("update.countdown",
                            function(event) {
                                var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                            });

                </script>

            <%
                }
            %>
        </article>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-3">
        <%
            var birthday5 = new DateTime(2017, 3, 16);
        %>
        <div>
            <h1>

                <i class="fa fa-code"></i>
                <%
                    if (DateTime.Now.Ticks < birthday5.Ticks)
                    {
                %>

                    Décompte avant les 33 ans de Dave-Gentil
                <%
                    }
                %>

            </h1>
            <div class="thumbnail">
                <img src="/Moi.jpg"/>
            </div>
        </div>
        <div>
            <article>

                <div class="example-blocks">
                    <div id="clock5" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
                </div>

                <%
                    if (DateTime.Now.Ticks > birthday5.Ticks)
                    {
                %>
                    <h1>Bonne Fête!! David</h1><br/>
                    <img src="cake.gif"/>
                <%
                    }
                    else
                    {
                %>

                    <script type="text/javascript">
                        var birthday5 = new Date(2017, 2, 16);
                        $("#clock5")
                            .countdown(birthday5)
                            .on("update.countdown",
                                function(event) {
                                    var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                                });

                    </script>

                <%
                    }
                %>
            </article>
        </div>
    </div>
    <div class="col-md-3">
        <%
            var birthday6 = new DateTime(2017, 8, 13);
        %>
        <div>
            <h1>

                <i class="fa fa-code"></i>
                <%
                    if (DateTime.Now.Ticks < birthday6.Ticks)
                    {
                %>

                    Décompte avant les 25 ans de Psycho Francesco
                <%
                    }
                %>

            </h1>
            <div class="thumbnail">
                <img src="/Francesco.png"/>
            </div>
        </div>
        <div>
            <article>

                <div class="example-blocks">
                    <div id="clock6" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
                </div>

                <%
                    if (DateTime.Now.Ticks > birthday6.Ticks)
                    {
                %>
                    <h1>Bonne Fête!! François</h1><br/>
                    <img src="cake.gif"/>
                <%
                    }
                    else
                    {
                %>

                    <script type="text/javascript">
                        var birthday6 = new Date(2017, 7, 13);
                        $("#clock6")
                            .countdown(birthday6)
                            .on("update.countdown",
                                function(event) {
                                    var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                                });

                    </script>

                <%
                    }
                %>
            </article>
        </div>
    </div>
    <div class="col-md-3">
        <%
            var birthday7 = new DateTime(2016, 12, 7);
        %>
        <div>
            <h1>

                <i class="fa fa-code"></i>
                <%
                    if (DateTime.Now.Ticks < birthday7.Ticks)
                    {
                %>

                    Décompte avant les 41 ans de L'ex Führer
                <%
                    }
                %>

            </h1>
            <div class="thumbnail">
                <img src="/Chris.png"/>
            </div>
        </div>
        <div>
            <article>

                <div class="example-blocks">
                    <div id="clock7" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
                </div>

                <%
                    if (DateTime.Now.Ticks > birthday7.Ticks)
                    {
                %>
                    <h1>Bonne Fête!! Chris</h1><br/>
                    <img src="cake.gif"/>
                <%
                    }
                    else
                    {
                %>

                    <script type="text/javascript">
                        var birthday7 = new Date(2016, 11, 7);
                        $("#clock7")
                            .countdown(birthday7)
                            .on("update.countdown",
                                function(event) {
                                    var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                                });

                    </script>

                <%
                    }
                %>
            </article>
        </div>
    </div>
</div>
<iframe width="420" height="315" src="https://www.youtube.com/embed/6EZl1LS8Gvc?autoplay=1" frameborder="0" allowfullscreen></iframe>
</div>

</main>
<footer>
</footer>
</body>
</html>