﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DavidLauzon30.Default" %>

<!DOCTYPE html>
<html>
<head>
    <title>30 ans! David Lauzon</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
    <script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.0.4/dist/jquery.countdown.min.js"></script>
</head>
<body>
<header>
</header>
<main>
    <% 
        var birthday = new DateTime(2015, 6, 4);
    %>
    <header class="content-header">
        <div class="container" style="color: #778899; text-align: center;">
            
            <h1><a href="defaut2.aspx">Ohhh mais que vois-je</a></h1>
            

            <h1>

                <i class="fa fa-code"></i>
                <%
                    if (DateTime.Now.Ticks > birthday.Ticks)
                    {
                        %>
                        
                            Décompte avant la fête du petit prince David adoré
                        <%
                    }
                    else
                    {
                        %>
                            30 ans!!!<br/>
                            <a href="default1.aspx">Mais ce n'est pas fini Cliquez ici</a>
                        <%
                    }
                 %>

            </h1>
            <img src="davidlauzon.jpg"/>
        </div>
    </header>
    <div class="container" style="text-align: center;">
        <div class="pure-g-r">

            <div class="examples pure-u-3-5
        ">
                <article>

                    <div class="example-blocks">
                        <div id="clock" style="font-size: 30pt; font-style: italic; font-weight: bold;"></div>
                    </div>

                        <%
                            if (DateTime.Now.Ticks < birthday.Ticks)
                            {
                        %>
                            <h1>Bonne Fête!! Dave</h1><br/>
                            <img src="cake.gif"/>
                        <%  
                            }
                            else
                            {

                            
                        %>
                        
                    <script type="text/javascript">
                        var birthday = new Date(2015, 6, 4);
                        $("#clock").countdown(birthday).on("update.countdown", function(event) {
                            var $this = $(this).html(event.strftime(event.strftime("%D jours %H:%M:%S")));
                        });
                        
                    </script>
                    <br/>
                    <img src="ashes.gif" alt="Tomber en poussiere"/>
                    
                        <%
                            }

                        %>
                </article>
            </div>

        </div>
    </div>

</main>
<footer>
</footer>
</body>
</html>